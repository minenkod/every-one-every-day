# Everyone Everyday NFC technology system

**Installing the application**

1. Connect the tablet to a computer and drag the APK into a familiar folder like Downloads.
2. With the APK on the tablet navigate to folder where the file was placed.
3. Tap on the APK and press install. A warning on installation from unknown sources may
    appear where you will need to allow installation.
4. After installation completes tap done.
**Using the application**
● Ensure the tablet is connected to internet and has a strong connection.
● On boot please wait 10-20 seconds for the tablet to synchronize with the database.
● Tapping an unregistered fob a registration screen will appear.
● Complete the registration form and press continue.
● Scan an unregistered tag for it to become registered.
● Tapping a registered tag navigates to the activity list.
● Selecting an activity will log a meeting for the user signed in.
● If the sign out activity is selected then a survey will be prompted, this can be skipped by
tapping sign me out.


**Building the application**
● Install Android studio for linux here:
https://developer.android.com/studio
● Using android studio open the project called: EE_Client 0.
● On the top of the screen scroll over to a menu called Build
● Next scroll over to Build Bundle(s) / APK(s)
● Click Build APK
At the bottom of the screen this will be a pop up. Please click locate and this window will open:
The file will be called app-debug.apk
You may rename this file to anything you wish.


**Changing API keys**
Changing the API key has to be done by following the instructions below.
● Install Android studio for linux here:
https://developer.android.com/studio
● Using android studio open the project called: EE_Client 0.
● On the left hand side click app -> readers.nfc.com.nfc_reader2 then click Uitls.


You will then navigated to this window
Here you can set the API key by replacing this line:

## static ​String ​ API_KEY ​= Cred.​ API_KEY ​; ​//Set API key here.

Replace all text inside the quotes to your new API key.

## public static ​String ​ API_KEY ​= ​"API_KEY_HERE"​;

Now please rebuild the APK using the instructions above to start using the new API key.



