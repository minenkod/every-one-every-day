package readers.nfc.com.nfc_reader2;

import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.browse.MediaBrowser;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.DatePicker;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import com.bumptech.glide.Glide;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static com.google.common.net.HttpHeaders.USER_AGENT;

//Activity aimed to register a user to the database. OK means sucessfull registration.

public class RegisterActivity extends AppCompatActivity {

    boolean allowedToScan = false;
//String formLink = wareHouse;
LinearLayout inputLayout;
   HubspotController   hubspotController;
WebView browser;
   boolean inputValid = false;
    ArrayList<String> signList;
    Utils utils;
    Button btnSubmit;
    Button exit;
    String EMAIL;
        TextView lblIntro;
    private Calendar myCalendar = Calendar.getInstance();
    private EditText etDate;
    TextView text;
    NfcAdapter nfcAdapter;
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        final ImageView gifImage = findViewById(R.id.thumbs_up);
        allowedToScan = false;
        if(!isNetworkAvailable())
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            Toast.makeText(this, "No Internet.", Toast.LENGTH_LONG).show();
            return;
        }
        String gifUrl = "https://media.giphy.com/media/d3MLEZoYiWR0J2TK/giphy.gif";
        Glide.with(getApplicationContext()).load(gifUrl).into(gifImage);
        browser  = findViewById(R.id.webview);
        hubspotController = new HubspotController();
        signList = new ArrayList<String>();
       // btnSubmit = (Button) findViewById(R.id.btnSubmit);
          utils = new Utils();
         final   LinearLayout successLayout =(LinearLayout) findViewById(R.id.successLayout);
         inputLayout =(LinearLayout) findViewById(R.id.inputLayout);
        lblIntro = findViewById(R.id.lblInto);
        exit = findViewById(R.id.btnExit);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        text = (TextView) findViewById(R.id.text);
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (nfcAdapter == null) {
            Toast.makeText(this, "No NFC", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        utils = new Utils();

        class MyJavaScriptInterface        /* An instance of this class will be registered as a JavaScript interface */
        {


            private TextView contentView;
            public MyJavaScriptInterface(TextView aContentView)
            {
                contentView = aContentView;
            }

            @SuppressWarnings("unused")
            @JavascriptInterface
            public void processContent(String aContent)
            {
                final String content = aContent;
                //Log.e("email",content);

                contentView.post(new Runnable()
                {
                    public void run()
                    {
                        if(!content.equals(""))
                        {
                            EMAIL = content;
                        }

                      //  contentView.setText(content);
                    }
                });
            }
            }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
       final String form =  preferences.getString("form", "https://share.hsforms.com/1xnzcKPSWSuaqzIMyBVP0gg2g8mw");
            browser.clearCache(true);
        browser.getSettings().setDomStorageEnabled(false);
        browser.getSettings().setJavaScriptEnabled(true);

        this.deleteDatabase("webview.db");
        this.deleteDatabase("webviewCache.db");

        browser.addJavascriptInterface(new MyJavaScriptInterface(text), "INTERFACE");
        browser.loadUrl(form);

        browser.setWebViewClient(new WebViewClient() {
            @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(!url.contains(form))
                {
                    if(url.contains("http://www.weareeveryone.org/privacy-policy"))
                    {
                        Toast.makeText(getApplicationContext(), "Please view the policy on a different device.", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Please try again.", Toast.LENGTH_LONG).show();
                    }
                    Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(intent);

                     return super.shouldOverrideUrlLoading(view, url);
                }
                browser.loadUrl("javascript:window.INTERFACE.processContent(document.getElementsByName('email')[0].value);");
                if(!EMAIL.equals(""))//Registration trigger.
                {
                    allowedToScan = true;
                  //  Toast.makeText(getApplicationContext(), "EMAIL IS: " + EMAIL, Toast.LENGTH_SHORT).show();
                    inputLayout.setVisibility(LinearLayout.GONE);

                    lblIntro.setVisibility(View.GONE);
                    text.setText("Now Scan A Tag!");
                    successLayout.setVisibility(LinearLayout.VISIBLE);
                    EMAIL = "";
                }
                clearCookies(getApplicationContext());
                return super.shouldOverrideUrlLoading(view, url);    //The webView is about to navigate to the specified url.
            }
        });
        browser.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                browser.loadUrl("javascript:window.INTERFACE.processContent(document.getElementsByName('email')[0].value);");
                return false;
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (nfcAdapter != null) {
            if (!nfcAdapter.isEnabled())
                showWirelessSettings();
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }

        private boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }



private void addSignIn(String id)
{
try {
    Intent intent = new Intent(getApplicationContext(), FinishRegActivity.class);
    if(allowedToScan){
        text.setText("");
        signList.add(id);
        //Get contact VID by email
        Thread.sleep(1000);
        String vid = "";
      boolean done = false;

        while(!done)
        {
            vid =   utils.getVid(EMAIL);
            Log.e("vid", EMAIL);
            if(!vid.equals(""))
            {
              done = true;
              continue;
            }
            Thread.sleep(100);

        }
        utils.addTagid(vid, id);
       DataHolder.getInstance().users.add(new Contact(vid, id));
        startActivity(intent);
    }
   else
    {
        Toast.makeText(this, "Please complete the form.", Toast.LENGTH_SHORT).show();
    }
}catch (Exception e){
 //   Log.e("aaaa", e.toString());
}
}

    private void showWirelessSettings() {
        Toast.makeText(this, "You need to enable NFC", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        resolveIntent(intent);
    }

    private void resolveIntent(Intent intent) {
        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;

            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];

                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                byte[] payload = utils.dumpTagData(tag).getBytes();
                addSignIn(utils.getHex(tag));
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, id, payload);
                NdefMessage msg = new NdefMessage(new NdefRecord[]{record});
                msgs = new NdefMessage[]{msg};
            }
            displayMsgs(msgs);
        }
    }

    private void displayMsgs(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0)
            return;

        StringBuilder builder = new StringBuilder();
        List<ParsedNdefRecord> records = NdefMessageParser.parse(msgs[0]);
        final int size = records.size();

        for (int i = 0; i < size; i++) {
            ParsedNdefRecord record = records.get(i);
            String str = record.str();
            builder.append(str).append("\n");
        }

    }


    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context)
    {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            Log.d("cc.", "Using clearCookies code for API >=" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else
        {
            Log.d("cc.", "Using clearCookies code for API <" + String.valueOf(Build.VERSION_CODES.LOLLIPOP_MR1));
            CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager=CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }


}
