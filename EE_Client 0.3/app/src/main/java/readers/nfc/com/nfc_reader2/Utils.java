package readers.nfc.com.nfc_reader2;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.os.HandlerThread;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.MultiAutoCompleteTextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

public class Utils {

static String API_KEY = Cred.API_KEY; //Set API key here.


    public String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    public String toReversedHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            if (i > 0) {
                sb.append(" ");
            }
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
        }
        return sb.toString();
    }

    public long toDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    public long toReversedDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    public String getHex(Tag tag) {
        StringBuilder sb = new StringBuilder();
        byte[] id = tag.getId();
        sb.append("").append(toHex(id));
        return sb.toString();
    }


    public String dumpTagData(Tag tag) {
        StringBuilder sb = new StringBuilder();
        byte[] id = tag.getId();
        sb.append("").append(toHex(id));
        sb.append("ID (reversed hex): ").append(toReversedHex(id)).append('\n');
        sb.append("ID (dec): ").append(toDec(id)).append('\n');
        sb.append("ID (reversed dec): ").append(toReversedDec(id)).append('\n');

        String prefix = "android.nfc.tech.";
        sb.append("Technologies: ");
        for (String tech : tag.getTechList()) {
            sb.append(tech.substring(prefix.length()));
            sb.append(", ");
        }

        sb.delete(sb.length() - 2, sb.length());

        for (String tech : tag.getTechList()) {
            if (tech.equals(MifareClassic.class.getName())) {
                sb.append('\n');
                String type = "Unknown";

                try {
                    MifareClassic mifareTag = MifareClassic.get(tag);

                    switch (mifareTag.getType()) {
                        case MifareClassic.TYPE_CLASSIC:
                            type = "Classic";
                            break;
                        case MifareClassic.TYPE_PLUS:
                            type = "Plus";
                            break;
                        case MifareClassic.TYPE_PRO:
                            type = "Pro";
                            break;
                    }
                    sb.append("Mifare Classic type: ");
                    sb.append(type);
                    sb.append('\n');

                    sb.append("Mifare size: ");
                    sb.append(mifareTag.getSize() + " bytes");
                    sb.append('\n');

                    sb.append("Mifare sectors: ");
                    sb.append(mifareTag.getSectorCount());
                    sb.append('\n');

                    sb.append("Mifare blocks: ");
                    sb.append(mifareTag.getBlockCount());
                } catch (Exception e) {
                    sb.append("Mifare classic error: " + e.getMessage());
                }
            }

            if (tech.equals(MifareUltralight.class.getName())) {
                sb.append('\n');
                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
                String type = "Unknown";
                switch (mifareUlTag.getType()) {
                    case MifareUltralight.TYPE_ULTRALIGHT:
                        type = "Ultralight";
                        break;
                    case MifareUltralight.TYPE_ULTRALIGHT_C:
                        type = "Ultralight C";
                        break;
                }
                sb.append("Mifare Ultralight type: ");
                sb.append(type);
            }
        }

        return sb.toString();
    }


static  ArrayList<Contact> contacts = new ArrayList<>();

    static private  ArrayList<Contact> extractContacts(JSONArray JsonCon) {
ArrayList<Contact> extracted = new ArrayList<>();
        try {
            for (int i = 0; i < JsonCon.length(); i++) {
                try {

                    String vid = "";
                    String tagid = "";
                    JSONObject jsonContact = JsonCon.getJSONObject(i);
                    if(jsonContact.has("vid")){
                        //get Value of video
                        vid = jsonContact.getString("vid");
                        tagid = jsonContact.getJSONObject("properties").getString("tagid");
                        tagid = tagid.substring(tagid.indexOf('"') + 1);
                        tagid = tagid.substring(tagid.indexOf('"') + 1);
                        tagid = tagid.substring(tagid.indexOf('"') + 1);
                        tagid = tagid.substring(0, tagid.indexOf('"'));
                        Log.e("Found-tagid", tagid);
                        extracted.add(new Contact(vid, tagid));
                    }


                } catch (Exception e) {
                    System.out.println("Exception " + e.toString());
                    continue;
                }
            }
        } catch (Exception e) {
        }
        return  extracted;
    }


static  private  ArrayList<Contact> addUniqueContacts(JSONArray jsonContacts)
{
    ArrayList<Contact> uniqueContacts = DataHolder.getInstance().users;
    try
    {
            try {
                ArrayList<Contact> recentContacts = extractContacts(jsonContacts);
                for(int i = 0; i< recentContacts.size(); i++)
                {
                    if(!uniqueContacts.contains(recentContacts.get(i)))
                    {
                        DataHolder.getInstance().users.add(new Contact (recentContacts.get(i).vid, recentContacts.get(i).idtag));
                    }
                }
            }
            catch (Exception e){
                System.out.println("Exception " + e.toString());
            }
        }

    catch (Exception e){
    }
    return DataHolder.getInstance().users;
}


    static public ArrayList<Contact> getUsers()
    {
        //contacts.clear();
        try
        {
                 String  strUrl =  "https://api.hubapi.com/contacts/v1/lists/all/contacts/all?hapikey=" + API_KEY +"&count=100&property=tagid";
                runJsonQueryAll(strUrl, "contacts");
            //  String response =    sendGet(strUrl);
        }
        catch (Exception e){return null;
        }
        return  DataHolder.getInstance().users;
    }



   static public ArrayList<Contact> getRecentUsers()
    {

       /// contacts.clear();
        try
        {
            final String strUrl = "https://api.hubapi.com/contacts/v1/lists/recently_updated/contacts/recent?hapikey=" + API_KEY + "&count=100&&property=tagid";
            runJsonQueryRecent(strUrl, "contacts");

            //  String response =    sendGet(strUrl);
        }
        catch (Exception e){return null;
        }
        return  DataHolder.getInstance().users;
    }


    static String resultingVid = "";


   static public void runJsonQueryRecent(final String request, final  String attr) //Get all contacts created in the last 30 days.
   {
       try {
           Thread t = new Thread(new Runnable() {
               public void run() {
                   String vidOffset = "";
                   String searchUrl = request;
                   try {

                           result = "";

                           InputStream stream = (InputStream) new URL(searchUrl).getContent();
                           BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                           String line = "";
                           while (line != null) {
                               result += line;
                               line = reader.readLine();
                           }
                           JSONObject json = new JSONObject(result);

                           //String ifMore  = json.getString("has-more");
                           vidOffset     = json.getString("vid-offset");
                           jsonContacts = json.getJSONArray(attr);
                            contacts =  addUniqueContacts(jsonContacts);

                   } catch (Exception e) {
                       e.printStackTrace();
                   }
               }
           });
           t.start();
           t.join();

       } catch (Exception e) {
       }

    //   return formattedResult;

   }




    public void addTagid(final String vid, final String tagid)
    {

               String json = "{\n" +
                       "\t\"properties\": [\n" +
                       "    {\n" +
                       "      \"property\": \"tagid\",\n" +
                       "      \"value\": \"" + tagid + '"' +
                       "    }\n" +
                       "    ]\n" +
                       "}";

                // Defined URL  where to send data
    String url = "https://api.hubapi.com/contacts/v1/contact/vid/" + vid + "/profile?hapikey=" + API_KEY;
                sendPost(url, json);

    }



    static public String getVid(String email) {
    final String url = "https://api.hubapi.com/contacts/v1/search/query?q=" + email + "&hapikey=" + API_KEY;
resultingVid = "";
    result = "";
        formattedResult = "";
        try {
            Thread t = new Thread(new Runnable() {
                public void run() {
                 try
                 {
                     InputStream stream = (InputStream) new URL(url).getContent();
                     BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                     String line = "";
                     while (line != null) {
                         result += line;
                         line = reader.readLine();
                     }
                     JSONObject json = new JSONObject(result);
                     JSONArray indentity  = json.getJSONArray("contacts");

                     JSONObject indentity2 = indentity.getJSONObject(0);
                     String vid = indentity2.getString("vid");

                    System.out.println(vid);
                     resultingVid = vid;
                 }catch (Exception e){
                     Log.e("s", e.toString());

                 }
                }

                 });
            t.start();
            t.join();
                }

                catch (Exception e){

                    Log.e("Error", e.toString());
                }
return  resultingVid;

    }


   static String result;
    static  String formattedResult;
  static   JSONArray jsonContacts;


static public String runJsonQueryAll(final String url, final String attr) {
    try {
        Thread t = new Thread(new Runnable() {
            public void run() {
                boolean hasMore = true;
                String vidOffset = "";
                String searchUrl = url;
                try {
                while(hasMore)
                {
                    result = "";

                    InputStream stream = (InputStream) new URL(searchUrl).getContent();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                    String line = "";
                    while (line != null) {
                        result += line;
                        line = reader.readLine();
                    }
                    JSONObject json = new JSONObject(result);

                  String ifMore  = json.getString("has-more");
                  vidOffset     = json.getString("vid-offset");
                    jsonContacts = json.getJSONArray(attr);
                  contacts = extractContacts(jsonContacts);
                    for(int i = 0; i < contacts.size(); i++)
                    {
                        DataHolder.getInstance().addUser(contacts.get(i).vid, contacts.get(i).idtag);
                    }
                    if(ifMore.equals("false"))
                    {
                        hasMore = false;

                    }
                    else if(ifMore.equals("true")){
                        result = "";
                        searchUrl = url + "&vidOffset=" + vidOffset;
                    }

                }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
        t.join();

    } catch (Exception e) {
    }

    return formattedResult;
}


    static public void sendPost(final String url, final String postData)

    {
        Thread postThread = new Thread("PostThread") {
            public void run() {
                try {

                    URL obj = new URL(url);
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                    //add reuqest header
                    con.setRequestMethod("POST");
                    con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    con.setRequestProperty("Accept", "application/json");
                    con.setDoOutput(true);
                    con.setDoInput(true);
                    // Send post request
                    con.setDoOutput(true);
                    DataOutputStream wr = new DataOutputStream(con.getOutputStream());


                    wr.write(postData.getBytes());
                    wr.flush();
                    //   wr.close();
                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'POST' request to URL : " + url);
                    //  System.out.println("Post parameters : " + urlParameters);
                    System.out.println("Response Code : " + responseCode);
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(con.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    System.out.println(response.toString());

                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        };
        postThread.start();

    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }
    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public void addMeeting(String activityName, String vid,Long tsLong, Long tfLong, String shopLocation) //Add meeting by inputing user id etc/
    {

        String ts = tsLong.toString();
        String tf = tfLong.toString();
        String postData = "{\n" +
                "    \"engagement\": {\n" +
                "        \"active\": true,\n" +
                "        \"ownerId\": 35563071,\n" +    //Ruchit 31703898
                "        \"type\": \"MEETING\",\n" +
                "        \"timestamp\": " + ts +"\n" +
                "    },\n" +
                "    \"associations\": {\n" +
                "        \"contactIds\": [" + vid + " ],\n" +
                "        \"companyIds\": [ ],\n" +
                "        \"dealIds\": [ ],\n" +
                "        \"ownerIds\": [ ]\n" +
                "    },\n" +
                "    \"attachments\": [\n" +
                "        {\n" +
                "            \"id\": 4241968539\n" +
                "        }\n" +
                "    ],\n" +
                "   \"metadata\": {\n" +
                "    \"body\": \"I used the NFC tap in system from " + shopLocation  + ".\",\n" +
                "    \"startTime\":" + ts + ",\n" +
                "    \"endTime\": " + tf + ",\n" +
                "    \"title\": \"" + activityName + "\"\n" +
                "  }\n" +
                "  \n" +
                "}\n";
        sendPost("https://api.hubapi.com/engagements/v1/engagements?hapikey=" + API_KEY, postData);
    }




}
