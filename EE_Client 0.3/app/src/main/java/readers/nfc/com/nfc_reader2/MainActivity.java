package readers.nfc.com.nfc_reader2;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
ArrayList<Contact> contacts;
NfcAdapter nfcAdapter;
Utils utils;
Button btn_register;
Button btn_activity;
HubspotController hubspotController;
Utils util;
ImageView logo;
private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkFirstRun();
        btn_activity = findViewById(R.id.btnUpActivity);
        hubspotController = new HubspotController();
        logo = findViewById(R.id.logo);
        logo.setImageResource(R.drawable.ee);
        btn_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), SelectingActivitiesActivity.class);
                startActivity(intent);

            }
        });
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (nfcAdapter == null) {
            Toast.makeText(this, "No NFC", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        pendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        utils = new Utils();

      if(DataHolder.getInstance().getCount() == 0)   //if no contacts added.
            {

           contacts =   util.getUsers();

            }

    //    contacts =   utils.getRecentUsers();
    }

    @Override
    public  void onBackPressed()
    {
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (nfcAdapter != null) {
            if (!nfcAdapter.isEnabled())
                showWirelessSettings();
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
        }
    }


    private void showWirelessSettings() {
        Toast.makeText(this, "You need to enable NFC", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        startActivity(intent);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
     resolveIntent(intent);
    }

    private void resolveIntent(Intent intent) {
        String action = intent.getAction();

        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;

            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];

                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }

            } else {

                byte[] empty = new byte[0];
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                byte[] payload = utils.dumpTagData(tag).getBytes();
                addSignIn(utils.getHex(tag));
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, id, payload);
                NdefMessage msg = new NdefMessage(new NdefRecord[]{record});
                msgs = new NdefMessage[]{msg};
            }

     displayMsgs(msgs);
        }

    }


    private void addSignIn(String id)
    {
        boolean found = false;
        String vid = "";


                System.out.println(DataHolder.getInstance().users.size());
                for(int i = 0; i< DataHolder.getInstance().users.size(); i++)
                {
                    Contact contact = DataHolder.getInstance().users.get(i);
                    if(contact.idtag.equals(id))
                    {
                     vid = contact.vid;
                    found = true;
                    }
                }

                if(found)
                {
                    Intent intent = new Intent(getApplicationContext(), SelectingActivitiesActivity.class);
                    intent.putExtra("vid", vid);
                    //SelectingActivitiesActivity
                    startActivity(intent);
                }
                else
                {
                    //Check recent
                    ArrayList<Contact> recentUsers = utils.getRecentUsers();
                    for(int  i =0; i< recentUsers.size(); i++)
                    {
                        if(recentUsers.get(i).idtag.equals(id)) {
                            Intent intent = new Intent(getApplicationContext(), SelectingActivitiesActivity.class);
                            intent.putExtra("vid", vid);
                            //SelectingActivitiesActivity
                            startActivity(intent);
                            return;
                        }

                    }
                    Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(intent);

                }

    }
    private void displayMsgs(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0)
            return;

        StringBuilder builder = new StringBuilder();
        List<ParsedNdefRecord> records = NdefMessageParser.parse(msgs[0]);
        final int size = records.size();

        for (int i = 0; i < size; i++) {
            ParsedNdefRecord record = records.get(i);
            String str = record.str();
            builder.append(str).append("\n");
        }

    }

    private void promptDialog() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);

        final ArrayList<String> hubspotForms = new ArrayList<>();
        String martinsCorner = "https://share.hsforms.com/1xnzcKPSWSuaqzIMyBVP0gg2g8mw";
        String churchElm =     "https://share.hsforms.com/16crOdYLRR76pvTImHf4QYA2g8mw";
        String wareHouse =     "https://share.hsforms.com/1ui8KjN68SL6q1vc1EvsYJA2g8mw";
        String rippleRoad =    "https://share.hsforms.com/1qHHQXj4BS1y3kVba2Mdpag2g8mw";

        hubspotForms.add(martinsCorner);
        hubspotForms.add(churchElm);
        hubspotForms.add(wareHouse);
        hubspotForms.add(rippleRoad);

       // /final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
      final ListView locationList = (ListView) mView.findViewById(R.id.locationList);
        ArrayList<String> locations = new ArrayList<>();
        locations.add("Martin's Corner Shop");
        locations.add("Church Elm Lane Shop");
        locations.add("Warehouse");
        locations.add("Ripple Road Shop");



        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, locations);
        locationList.setAdapter(adapter);


        locationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = adapter.getItem(position);
                //     Toast.makeText(getApplicationContext(), "Tap SET to confirm and choose the " + value + " location.", Toast.LENGTH_LONG).show();
            //    Toast.makeText(getApplicationContext(), hubspotForms.get( position), Toast.LENGTH_LONG).show();
                setDefaults("form", hubspotForms.get( position), getApplicationContext() );
                setDefaults("location",value, getApplicationContext() );
            }
        });

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        setDefaults("FR", "Y", getApplicationContext()); //Disable first run.
                        Toast.makeText(getApplicationContext()  , "Thank you", Toast.LENGTH_LONG).show();
                    }
                });



        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }
    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private void checkFirstRun(){

        String form = getDefaults("form", getApplicationContext());
        String location = getDefaults("location", getApplicationContext());

      //  Toast.makeText(this , "My form URL is: " + form + " & my location " + location, Toast.LENGTH_LONG ).show();

        if (!getDefaults("FR", getApplicationContext()).equals("Y")) {
            //the app is being launched for first time, do something

            //ask questions on
            promptDialog();


            // first time task

            // record the fact that the app has been started at least once

        }
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, "N");
    }
}
