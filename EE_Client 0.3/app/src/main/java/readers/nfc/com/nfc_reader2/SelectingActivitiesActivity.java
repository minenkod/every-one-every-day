package readers.nfc.com.nfc_reader2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.util.Util;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.StandardSocketOptions;
import java.net.URL;
import java.sql.Struct;
import java.util.ArrayList;

public class SelectingActivitiesActivity extends AppCompatActivity {

    Button btnSet;
    ImageView i;
    HubspotController hubspotController;
String vid;
private ArrayList<String> mNames = new ArrayList<String >();
private  ArrayList<String> mImageUrls = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selecting_activities);
        btnSet = findViewById(R.id.btnSet);
        vid = getIntent().getStringExtra("vid");
        //   imageView = findViewById(R.id.img);

        if(!isNetworkAvailable())
        {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            Toast.makeText(this, "No Internet.", Toast.LENGTH_LONG).show();
            return;
        }

        initImageBitmaps();
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

    }

    Bitmap bitmap = null;

    @Override
    public  void onBackPressed()
    {


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
private void initImageBitmaps()
{

mNames.add("I'm just dropping in");
mImageUrls.add("https://static1.squarespace.com/static/59fe22186957da47865ee364/5b255a0503ce643d6e39abac/5b255eb4f950b7fedf01b2a9/1550671778804/bigstock-Close-Up-Table-With-Delicious--233271967.jpg?format=1000w");
mNames.add("I'm here to plan a project");
mImageUrls.add("https://static1.squarespace.com/static/59fe22186957da47865ee364/5b256470562fa7107c8f2dd4/5c6c2464ec212d282bfb9184/1550671838054/bigstock-Little-asian-girl-lying-on-flo-203276197.jpg?format=750w");
mNames.add("I'm here for an activity.");
mImageUrls.add("https://static1.squarespace.com/static/59fe22186957da47865ee364/5b256bc1575d1f6f79326965/5b296f882b6a28e9f8455532/1550674673767/29214722_378331529300091_1836118315272589051_n.jpg?format=500w");
mNames.add("I'm signing out");
mImageUrls.add("https://static1.squarespace.com/static/59fe22186957da47865ee364/5b25613c88251ba762ce18e4/5c6d3f43652deaed87b763a1/1550672469130/sunflower.jpg?format=1000w");


initRecyclerView();
}

private  void initRecyclerView()
{
    RecyclerView recyclerView = findViewById(R.id.recycleV );
    RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls, vid);
    recyclerView.setAdapter(adapter);
recyclerView.setLayoutManager(new LinearLayoutManager(this));
}
private void imageThread() {   //Downloading an image from the internet thread.
        Thread t;
        t = new Thread(){
            @Override
            public void run() {

                synchronized (this) {
                        try {
                        String urlLink = "https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260";
                            bitmap  = BitmapFactory.decodeStream((InputStream)new URL(urlLink).getContent());
                        }catch (Exception e){  System.out.println(e.toString()); }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                  i.setImageBitmap(bitmap);
                            }
                        });

                    }
            }
        };
        t.start();

    }
}
