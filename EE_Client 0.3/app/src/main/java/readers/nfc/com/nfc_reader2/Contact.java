package readers.nfc.com.nfc_reader2;

public class Contact {
    public String vid;
    public   String idtag;

    public Contact(String vid, String idtag) {
        this.idtag = idtag;
        this.vid = vid;
    }
}
