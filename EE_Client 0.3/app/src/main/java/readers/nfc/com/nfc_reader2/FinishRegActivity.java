package readers.nfc.com.nfc_reader2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class FinishRegActivity extends AppCompatActivity {
//https://media.giphy.com/media/l0IyccEqUTO7xy9Ec/giphy.gif
Button btnFinish;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_reg);
        final ImageView gifImage = findViewById(R.id.thumbs_up);


        String gifUrl = "https://media.giphy.com/media/l41lUjUgLLwWrz20w/giphy.gif";
        Glide.with(this).load(gifUrl).into(gifImage);
        btnFinish = findViewById(R.id.btnFinish);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
               intent.putExtra("refresh", "true");

                startActivity(intent);
            }
        });
    }
}
